# Slang Panameño CLI 🇵🇦 Que xopa! <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px">

> 🔸 versión temprana y en desarrollo

# Bienvenido al CLI de slang panameño

## ¿Qué es esto?

### Este es un proyecto de código abierto que lleva a cabo la conversión del slang panameño a una API en donde cualquiera persona pude hacer sus aportaciones de palabras. Lo puedes ver [aquí!](https://slangpanameno.herokuapp.com/slang)

### Este CLI busca de una manera más cómoda  ingresar,editar,eliminar nuevo datos a la [API](https://gitlab.com/RichardQuiros/slang-panameno-api) ademas de poder almacenar los datos de forma local en un database esta es la versión  [sql3](https://www.sqlite.org/index.html).

## Versión ORM

### En esta versión se utiliza un ORM para hacer el crud y permitir más dialectos: SQLite,MariaDB. Por defecto usará SQLite al menos que agregues la bandera -mariadb o -m donde te pedirá  las credenciales.

## Versión MONGO

### En esta versión se implementa mongodb para poder crear una copia de la api en una base de datos no relacional y poder hacer el crud desde el cli.Por defecto usará SQLite al menos que agregues la bandera -mongodb o -mo donde te pedirá las credenciales.

## Versión REDIS

### En esta versión se implementa redisdb para poder crear una copia de la api en una base de datos no relacional y poder hacer el crud desde el cli.Por defecto usará SQLite al menos que agregues la bandera -redisdb o -r donde te pedirá las credenciales.

## Versión UI

### En esta versión se creo una UI con flask que interactua directamente con el cli
>> Para utilizarlo dirígete a ./UI/main.py y ejecutalo 


# Instalación
>Primero debes de instalar las librerias que se utilizaron
>>$ pip install 
# Uso
 ### si quieres obtener ayuda
 ```$ python main.py --help``` 
 
 ### esto te devolvera todos los comando con un comentario de lo que realizan

 ### si quieres leer mas a detalle el comentario basta con el comando + --help
```$ python main.py get-all --help``` 

## Hay dos cosas que hace este CLI el CRUD de API y de tabla/database

### Para la API
#### Puedes 

<details>
  <summary>Ver comandos</summary>
  <details>
  <summary>delete-by-id</summary>

##### elimina data de la API que concidan con la id requiere <id>
```$ python main.py delete-by-id 10```
  </details>

  <details>
  <summary> get-all </summary>

##### obtiene todos los datos de la API
```$ python main.py get-all```
  </details>

  <details>
  <summary> get-by-id </summary>

##### obtiene el slang de la API que concidan con el id - requiere <id>
```$ python main.py get-by-id 10```
  </details>

<details>
  <summary> post-slang </summary>

##### añade un nuevo slang a la API - requiere <id> <slang/palabra> <definicion> opcional:--ejemplo <ejemplo> 
   
```$ python main.py post-slang 10 'nuevo slang' 'definicion etc..' --ejemplo 'algun ejemplo'```
</details>


<details>
  <summary> put-definicion </summary>

##### actualiza data de la API solamente definicion requiere <id> <definicion>
```$ python main.py put-definicion 10 'definicion etc...'```
</details>


<details>
  <summary> put </summary>

##### actualiza data de la API - requiere <id> <slang/palabra> <definicion> opcional: --ejemplo <ejemplo>
```$ python main.py put 10 'nuevo slang' 'definicion etc..' --ejemplo 'algun ejemplo'```
</details>


<details>
  <summary> put-ejemplo </summary>

##### actualiza data de la API solamente ejemplo requiere <id> <ejemplo>
```$ python main.py put-ejemplo 10 'algun ejemplo'```
</details>


<details>
  <summary> put-slang </summary>

##### actualiza data de la API solamente slang/palabra requiere <id> <slang/palabra>
```$ python main.py put-slang 10 'Cambio de slang'```
</details>
 
</details>



### Para la Tabla/database
#### Puedes 
> Utiliza la Bandera --mariadb o -m para utilizar MariaDB
>> $ python main.py db-create 'myTableName' -m

> Utiliza la Bandera --mongodb o -mo para utilizar MongoDB
>> $ python main.py db-create 'mydbName' -mo

> Utiliza la Bandera --redisdb o -r para utilizar RedisDB
>> $ python main.py db-create 'mydbName' -r

<details>
  <summary>Ver comandos</summary>
 <details>
  <summary> db-create </summary>

##### crea una data base local con sql3 requiere <nombreDeLaTabla/db>
```$ python main.py db-create nombreDeLaTabla```
</details>


<details>
  <summary> db-delete </summary>

##### elimina todo de la tabla/database requiere <nombreDeLaTabla/db>
```$ python main.py db-delete nombreDeLaTabla```
</details>


<details>
  <summary> db-delete-by-id </summary>

##### elimina datos que concida con el slang/palabra a la tabla/database requiere <nombreDeLaTabla/db> <id>
```$ python main.py db-delete-by-id nombreDeLaTabla 10```
</details>

<details>
  <summary> db-get </summary>

##### obtiene los datos de la tabla/database requiere <nombreDeLaTabla/db>
```$ python main.py db-get nombreDeLaTabla```
</details>


<details>
  <summary> db-get-by-id </summary>

##### obtiene los datos de la tabla/database que concidan con la id requiere <nombreDeLaTabla/db> <id>
```$ python main.py db-get-by-id nombreDeLaTabla 10```
</details>


<details>
  <summary> db-get-by-slang </summary>

##### obtiene los datos de la tabla/database que concidan con el slang/palabra requiere <nombreDeLaTabla/db> <slang>
```$ python main.py db-get-by-slang nombreDeLaTabla 'birria'```
</details>

<details>
  <summary> db-post </summary>

##### añade un nuevo slang/palabra a la tabla/database requiere <nombreDeLaTabla/db> <id> <slang/palabra> <definicion> opcional: --ejemplo <ejemplo>
```$ python main.py db-post nombreDeLaTabla 99 'nuevo slang' 'la definicion etc..' --ejemplo 'algun ejemplo'```
</details>


<details>
  <summary> db-put </summary>

##### actualiza datos que concida con el id a la tabla/database requiere <nombreDeLaTabla/db> <id> <slang/palabra> <definicion> opcional: --ejemplo <ejemplo>
```$ python main.py db-put nombreDeLaTabla 10 'changed slang' 'la definicion etc..' --ejemplo 'algun ejemplo'```
</details>

<details>
  <summary> db-put-slang </summary>

##### actualiza slang/palabra que concida con el id a la tabla/database requiere <nombreDeLaTabla/db> <id> <slang/palabra> opcional: --ejemplo <ejemplo>
```$ python main.py db-put-slang nombreDeLaTabla 10 'changed slang'```
</details>



<details>
  <summary> db-put-definicion </summary>

##### actualiza definicion que concida con el id a la tabla/database requiere <nombreDeLaTabla/db> <id>  <definicion>opcional: --ejemplo <ejemplo>
```$ python main.py db-put-definicion nombreDeLaTabla 10  'la definicion etc..' ```
</details>

<details>
  <summary> db-put-ejemplo </summary>

##### actualiza ejemplo que concida con el id a la tabla/database requiere <nombreDeLaTabla/db> <id> <ejemplo>opcional: --ejemplo <ejemplo>
```$ python main.py db-put-ejemplo nombreDeLaTabla 10 'algun ejemplo'```
</details>

<details>
  <summary> db-refresh </summary>

##### refresca la tabla/database con los datos de la API requiere <nombreDeLaTabla/db>
```$ python main.py db-refresh nombreDeLaTabla```
</details>
 
</details>

- ### nota
>si quieres usar cualquier comando de table debes tener por lo menos alguna tabla creada y simpre poner el nombre de esa tabla como primer parametro
>>```$ python main.py db-create myTableName ```
>>```$ python main.py db-refresh myTableName ```

### otros comando
<details>
<summary> ver otros comandos </summary>


<details>
  <summary> open-api </summary>

##### abre el REST API
```$ python main.py open-api```
</details>

<details>
  <summary> open-git </summary>

##### abre el repositorio de git 
```$ python main.py open-git```
</details>

<details>
  <summary> open-ui </summary>

##### en prosceso... ⚠
```$ python main.py open-ui```
</details>

<details>
  <summary> ver-logo </summary>

##### ver logo y version 
```$ python main.py ver-logo```
</details>
</details>

# repositorio API [aquí](https://gitlab.com/RichardQuiros/slang-panameno-api)
# 💢 Inconvenientes
#### ‣ La API actualmente no dispone de un servidor estable y esta hostiado por la versión gratuita de desarrollador de [HEROKU](https://dashboard.heroku.com)
#### ‣ Falta pulir y mejorar la API como ejemplo añadir COORS, organización de los datos o hacer que reciba request with parameters body.
#### ‣ Falta cumplir los requerimientos anteriores para comenzar con la ui
# Por último  decirles que es un proyecto totalmente open source 🎊🎉
##### Puedes encontrarme en instagram como [@rquiros7255](https://www.instagram.com/quiros7255/?hl=en) <img src="https://cdn4.iconfinder.com/data/icons/materia-social-free/24/038_011_instagram_mobile_photo_network_android_material-128.png" width="20"/>

@RichardQuiros
