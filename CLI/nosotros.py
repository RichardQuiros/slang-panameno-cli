import webbrowser

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[34m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    RED = '\33[31m'
    UNDERLINE = '\033[4m'

def logo():
    print(bcolors.OKBLUE +"""

          ██████  ██▓    ▄▄▄       ███▄    █   ▄████     ██▓███   ▄▄▄       ███▄    █  ▄▄▄       ███▄ ▄███▓▓█████  ███▄    █  ▒█████  
        ▒██    ▒ ▓██▒   ▒████▄     ██ ▀█   █  ██▒ ▀█▒   ▓██░  ██▒▒████▄     ██ ▀█   █ ▒████▄    ▓██▒▀█▀ ██▒▓█   ▀  ██ ▀█   █ ▒██▒  ██▒ """+ bcolors.ENDC+"""
        ░ ▓██▄   ▒██░   ▒██  ▀█▄  ▓██  ▀█ ██▒▒██░▄▄▄░   ▓██░ ██▓▒▒██  ▀█▄  ▓██  ▀█ ██▒▒██  ▀█▄  ▓██    ▓██░▒███   ▓██  ▀█ ██▒▒██░  ██▒
          ▒   ██▒▒██░   ░██▄▄▄▄██ ▓██▒  ▐▌██▒░▓█  ██▓   ▒██▄█▓▒ ▒░██▄▄▄▄██ ▓██▒  ▐▌██▒░██▄▄▄▄██ ▒██    ▒██ ▒▓█  ▄ ▓██▒  ▐▌██▒▒██   ██░"""+ bcolors.RED + """
        ▒██████▒▒░██████▒▓█   ▓██▒▒██░   ▓██░░▒▓███▀▒   ▒██▒ ░  ░ ▓█   ▓██▒▒██░   ▓██░ ▓█   ▓██▒▒██▒   ░██▒░▒████▒▒██░   ▓██░░ ████▓▒░
        ▒ ▒▓▒ ▒ ░░ ▒░▓  ░▒▒   ▓▒█░░ ▒░   ▒ ▒  ░▒   ▒    ▒▓▒░ ░  ░ ▒▒   ▓▒█░░ ▒░   ▒ ▒  ▒▒   ▓▒█░░ ▒░   ░  ░░░ ▒░ ░░ ▒░   ▒ ▒ ░ ▒░▒░▒░ 
        ░ ░▒  ░ ░░ ░ ▒  ░ ▒   ▒▒ ░░ ░░   ░ ▒░  ░   ░    ░▒ ░       ▒   ▒▒ ░░ ░░   ░ ▒░  ▒   ▒▒ ░░  ░      ░ ░ ░  ░░ ░░   ░ ▒░  ░ ▒ ▒░ 
        ░  ░  ░    ░ ░    ░   ▒      ░   ░ ░ ░ ░   ░    ░░         ░   ▒      ░   ░ ░   ░   ▒   ░      ░      ░      ░   ░ ░ ░ ░ ░ ▒  
            ░      ░  ░     ░  ░         ░       ░                   ░  ░         ░       ░  ░       ░      ░  ░         ░     ░ ░  
                                                                                                                                    
        -------------------------------------------------------------------------------------------------------------------------------
        """)
    print(bcolors.OKBLUE+"CLI SLANG PANAMEÑO")
    print(bcolors.OKBLUE+"0.0.1v")


def openApi():
    webbrowser.open("https://slangpanameno.herokuapp.com/slang/")

def openGit():
    webbrowser.open("https://gitlab.com/RichardQuiros/slang-panameno-cli")

def openUI():
    webbrowser.open("https://gitlab.com/RichardQuiros/slang-panameno-api/-/tree/master/UI")

