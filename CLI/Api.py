# -*- coding: utf-8 -*-
import requests
import json

url = 'https://slangpanameno.herokuapp.com/slang/'
def getApi():
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()

def getApiByID(id):
    response = requests.get(url+id)
    if response.status_code == 200:
        return response.json()


def postApi(id,slang,definicion,ejemplo):
    payload = {'slang':slang,'definicion':definicion,'ejemplo':ejemplo}
    response = requests.post(url+str(id),json=payload)
    if response.status_code == 201:
        print("Cambio exitos los cambios se aplicaran dentro de poco tiempo ✨")
    elif  response.status_code == 400:
        print("No se pudo completar la accion tal vez no se ha completado su peticion anterior⛔")
             
def putApi(id,slang,definicion,ejemplo):
    payload = {'slang':slang,'definicion':definicion,'ejemplo':ejemplo}
    response = requests.put(url+id,json=payload)
    if response.status_code == 200:
        print("Cambio exitos ✨")
    else: 
        print("No se pudo completar la accion ⛔")

def putApiSlang(id,slang):
    data = getApiByID(id)
    payload = {'slang':slang,'definicion':data["definicion"],'ejemplo':data["ejemplo"]}
    response = requests.put(url+id,json=payload)
    if response.status_code == 200:
        print("Cambio exitos ✨")
    else: 
        print("No se pudo completar la accion ⛔")

def putApiDefinicion(id,definicion):
    data = getApiByID(id)
    payload = {'slang':data["slang"],'definicion':definicion,'ejemplo':data["ejemplo"]}
    response = requests.put(url+id,json=payload)
    if response.status_code == 200:
        print("Cambio exitos ✨")
    else: 
        print("No se pudo completar la accion ⛔")

def putApiejemplo(id,ejemplo):
    data = getApiByID(id)
    payload = {'slang':data["slang"],'definicion':data["ejemplo"],'ejemplo':ejemplo}
    response = requests.put(url+id,json=payload)
    if response.status_code == 200:
        print("Cambio exitos ✨")
    else: 
        print("No se pudo completar la accion ⛔")

def deleteByID(id):
    response = requests.delete(url+id)
    if response.status_code == 200:
         print("Slang eliminado ❌")
    else: 
        print("No se pudo completar la accion ⛔")
         
def getLenApi():
    number = getApi()
    return len(number)

#postApi("55","asdsa","asdsad","asedasd")
#print(getLenApi()+1)
#updateCache(50)
#print(getLenApiCache()[1])
#print(val[0])