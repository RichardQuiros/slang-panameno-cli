# -*- coding: utf-8 -*-
from Api import getApi,getLenApi
from Slang import Slang
import sqlite3
class SQL:
    def __init__(self,nameTable):
        self.nameTable = nameTable+'.db'
        self.sql = sqlite3.connect(self.nameTable)
        self.cursor = self.sql.cursor()

    

    def initTable(self):
        try:
            self.cursor.execute("""CREATE TABLE Slang(
                        id text,
                        slang text,
                        definicion text,
                        ejemplo text)"""
                        ) 
            self.refreshTable()
            print('Tabla creado con exito✨')
        except sqlite3.OperationalError:
            print('la tabla ya existe ❌')
       

    def refreshTable(self):
        data = getApi()
        self.deleteAll()
        try:
            with self.sql:
                for info in data:
                    self.cursor.execute("INSERT INTO Slang VALUES (:id,:slang,:definicion,:ejemplo)", 
                     {'id':info["id"],'slang':info["slang"],'definicion':info["definicion"],'ejemplo':info["ejemplo"]})
                print('La tabla esta refrescada✨')
        except sqlite3.OperationalError:
            print('la tabla no ha sido inicializada ❌')
        
    def postSlang(self,id,slang,definicion,ejemplo):
        try:
            with self.sql:
              self.cursor.execute("INSERT INTO Slang VALUES (:id,:slang,:definicion,:ejemplo)", 
                {'id':id,'slang':slang,'definicion':definicion,'ejemplo':ejemplo})
            print('Se a insertado con exito✨')
        except sqlite3.OperationalError:
            print('la tabla no ha sido inicializada ❌')
        
            

    def getSlangById(self,id):
        try:
             self.cursor.execute("SELECT * FROM Slang WHERE id=:id",{'id':id})
             return self.cursor.fetchall()
        except sqlite3.OperationalError:
            print('la tabla no ha sido inicializada ❌')
        

    def getSlangBySlang(self,slang):
        try:
            self.cursor.execute("SELECT * FROM Slang WHERE slang=:slang",{'slang':slang})
            return self.cursor.fetchall()
        except sqlite3.OperationalError:
            print('la tabla no ha sido inicializada ❌')
       

    def getAllSlang(self):
        try:
            self.cursor.execute("SELECT * FROM Slang")
            return self.cursor.fetchall()
        except sqlite3.OperationalError:
            print('la tabla no ha sido inicializada ❌')
        

    def deleteSlangById(self,id):
        try:
            with self.sql:
                        self.cursor.execute("DELETE FROM Slang WHERE id=:id",{'id':id})
                        print('Borrado con exito✨')
        except sqlite3.OperationalError:
            print('la tabla no ha sido inicializada ❌')
    
    def deleteSlangBySlang(self,slang):
        try:
            with self.sql:
                        self.cursor.execute("DELETE FROM Slang WHERE slang=:slang",{'slang':slang})
                        print('Borrado con exito✨')
        except sqlite3.OperationalError:
            print('la tabla no ha sido inicializada ❌')

    def deleteAll(self):
        try:
            with self.sql:
             self.cursor.execute("DELETE FROM Slang")
             print('Todo limpio✨')
        except sqlite3.OperationalError:
            print('la tabla no ha sido inicializada ❌')
        

    def put(self,id,slang,definicion,ejemplo):
         try:
            with self.sql:
                self.cursor.execute("""UPDATE Slang SET slang = :slang , definicion = :definicion , ejemplo = :ejemplo
                WHERE id = :id""",{'id':id,'slang':slang,'definicion':definicion,'ejemplo':ejemplo})
            print('update con exito✨')
         except sqlite3.OperationalError:
            print('No existe ❌')

     
    def putSlang(self,id,slang):
         try:
             with self.sql:
                self.cursor.execute("""UPDATE Slang SET slang = :slang
                            WHERE id = :id""",{'id':id,'slang':slang}) 
                print('update con exito✨')          
         except sqlite3.OperationalError:
            print('No existe esa id ❌')

        

    def putDefinicion(self,id,definicion):
         try:
             with self.sql:
                 self.cursor.execute("""UPDATE Slang SET definicion = :definicion
                    WHERE id = :id""",{'id':id,'definicion':definicion})   
             print('update con exito✨')        
         except sqlite3.OperationalError:
            print('No existe esa id ❌')
        

    def putEjemplo(self,id,ejemplo):
         try:
            with self.sql:
             self.cursor.execute("""UPDATE Slang SET ejemplo = :ejemplo
                            WHERE id = :id""",{'id':id,'ejemplo':ejemplo})
            print('Tabla creado con exito✨')             
         except sqlite3.OperationalError:
            print('No existe esa id ❌')
                      
    def closeTable(self):
        self.sql.close()

