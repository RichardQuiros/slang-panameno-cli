# -*- coding: utf-8 -*-
#!/usr/bin/env python
# chmod +x main.py
#python main.py get_all
#python main.py 
import click
from Table import SQL
from Api import postApi,deleteByID,putApi,putApiSlang,putApiejemplo,putApiDefinicion,getApi,getApiByID
from nosotros import logo,openApi,openGit,openUI
@click.group() 
def main():
    pass
 
#OPCIONES DE API

@main.command()
@click.argument('id')
@click.argument('slang')
@click.argument('definicion')
@click.option('--ejemplo','-e',help='algun ejemplo',default='')
def post_slang(id,slang,definicion,ejemplo):
    "añade un nuevo slang a la API - requiere <id> <slang/palabra> <definicion> opcional:--ejemplo <ejemplo> "
    postApi(id,slang,definicion,ejemplo)

@main.command()
def get_all():
    """obtiene todos los datos de la API"""
    print(getApi())

@main.command()
@click.argument('id')
def get_by_id(id):
    "obtiene el slang de la API que concidan con el id - requiere <id>"
    print(getApiByID(id))

@main.command()
@click.argument('id')
@click.argument('slang')
@click.argument('definicion')
@click.option('--ejemplo','-e',help='algun ejemplo',default='')
def put(id,slang,definicion,ejemplo):
    "actualiza data de la API - requiere <id> <slang/palabra> <definicion> opcional: --ejemplo <ejemplo>"
    putApi(id,slang,definicion,ejemplo)

@main.command()
@click.argument('id')
@click.argument('slang')
def put_slang(id,slang):
    "actualiza data de la API solamente slang/palabra requiere <id> <slang/palabra>"
    putApiSlang(id,slang)

@main.command()
@click.argument('id')
@click.argument('definicion')
def put_definicion(id,definicion):
    "actualiza data de la API solamente definicion requiere <id> <definicion>"
    putApiDefinicion(id,definicion)

@main.command()
@click.argument('id')
@click.argument('ejemplo')
def put_ejemplo(id,ejemplo):
    "actualiza data de la API solamente ejemplo requiere <id> <ejemplo>"
    putApiejemplo(id,ejemplo)


@main.command()
@click.argument('id')
def delete_by_id(id):
    "elimina data de la API que concidan con la id requiere <id>"
    deleteByID(id)

#OPCIONES SOBRE NOSOTROS

@main.command()
def open_api():
    "abre el REST API"
    openApi()

@main.command()
def open_git():
    "abre el repositorio de git"
    openGit()

@main.command()
def open_ui():
    "en prosceso... ⚠"
    openUI()
    print('muy pronto... ⚠')

@main.command()
def ver_logo():
    "ver logo y version"
    logo()

#OPCIONES DE TABLA "IMPORTANTES EN ESTAS OPCIONES ES IMPORTATE QUE CREES LA TABLA PRIMERO"

@main.command()
@click.argument('table')
def table_create(table):
    "crea una data base local con sql3 requiere <nombreDeLaTabla/db>"
    table = SQL(table)
    table.initTable()
    table.closeTable()

@main.command()
@click.argument('table')
def table_refresh(table):
    "refresca la tabla/database con los datos de la API requiere <nombreDeLaTabla/db>"
    val = input('Si refresca se perdera los datos que sean locales. desea continuar [y/n]')
    if val=='y' or val =='Y' or val=='yes' or val=='Yes' or val=='YES': 
        table = SQL(table)
        table.refreshTable()
        table.closeTable()
    else:
        print('CANCELADO⛔')

@main.command()
@click.argument('table')
def table_get(table):
    "obtiene los datos de la tabla/database requiere <nombreDeLaTabla/db>"
    table = SQL(table)
    print(table.getAllSlang())
    table.closeTable()

@main.command()
@click.argument('table')
@click.argument('id')
def table_get_by_id(table,id):
    "obtiene los datos de la tabla/database que concidan con la id requiere <nombreDeLaTabla/db> <id>"
    table = SQL(table)
    print(table.getSlangById(id))
    table.closeTable()

@main.command()
@click.argument('table')
@click.argument('slang')
def table_get_by_slang(table,slang):
    "obtiene los datos de la tabla/database que concidan con el slang/palabra requiere <nombreDeLaTabla/db> <slang>"
    table = SQL(table)
    print(table.getSlangBySlang(slang))
    table.closeTable()

@main.command()
@click.argument('table')
@click.argument('id')
@click.argument('slang')
@click.argument('definicion')
@click.option('--ejemplo','-e',help='algun ejemplo',default='')
def table_post(table,id,slang,definicion,ejemplo):
    "añade un nuevo slang/palabra a la tabla/database requiere <nombreDeLaTabla/db> <id> <slang/palabra> <definicion> opcional: --ejemplo <ejemplo>"
    table = SQL(table)
    table.postSlang(id,slang,definicion,ejemplo)
    table.closeTable()

@main.command()
@click.argument('table')
@click.argument('id')
@click.argument('slang')
@click.argument('definicion')
@click.option('--ejemplo','-e',help='algun ejemplo',default='')
def table_put(table,id,slang,definicion,ejemplo):
    "actualiza datos que concida con el id a la tabla/database requiere <nombreDeLaTabla/db> <id> <slang/palabra> <definicion> opcional: --ejemplo <ejemplo>"
    table = SQL(table)
    table.put(id,slang,definicion,ejemplo)
    table.closeTable()

@main.command()
@click.argument('table')
@click.argument('id')
@click.argument('slang')
def table_put_slang(table,id,slang):
    "actualiza slang/palabra que concida con el id a la tabla/database requiere <nombreDeLaTabla/db> <id> <slang/palabra>"
    table = SQL(table)
    table.putSlang(id,slang)
    table.closeTable()

@main.command()
@click.argument('table')
@click.argument('id')
@click.argument('definicion')
def table_put_definicion(table,id,definicion):
    "actualiza definicion que concida con el id a la tabla/database requiere <nombreDeLaTabla/db> <id>  <definicion> "
    table = SQL(table)
    table.putDefinicion(id,definicion)
    table.closeTable()

@main.command()
@click.argument('table')
@click.argument('id')
@click.argument('ejemplo')
def table_put_ejemplo(table,id,ejemplo):
    "actualiza ejemplo que concida con el id a la tabla/database requiere <nombreDeLaTabla/db> <id> <ejemplo>"
    table = SQL(table)
    table.putEjemplo(id,ejemplo)
    table.closeTable()

@main.command()
@click.argument('table')
@click.argument('slang')
def table_delete_by_slang(table,slang):
    "elimina datos que concida con el slang/palabra a la tabla/database requiere <nombreDeLaTabla/db> <slang/palabra>"
    table = SQL(table)
    table.deleteSlangBySlang(slang)
    table.closeTable()

@main.command()
@click.argument('table')
@click.argument('id')
def table_delete_by_id(table,id):
    "elimina datos que concida con el id a la tabla/database requiere <nombreDeLaTabla/db> <id>"
    table = SQL(table)
    table.deleteSlangById(id)
    table.closeTable()

@main.command()
@click.argument('table')
def table_delete(table):
    "elimina todo de la tabla/database requiere <nombreDeLaTabla/db>"
    val = input('Se perdera tod la informacion local! desea continuar [y/n]')
    if val=='y' or val =='Y' or val=='yes' or val=='Yes' or val=='YES': 
        table = SQL(table)
        table.deleteAll()
        table.closeTable()
    else:
        print('CANCELADO⛔')
    



if __name__ == '__main__':
    main()
